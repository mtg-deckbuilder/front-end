import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardListComponent } from './card-list/card-list.component';
import {CardServiceService} from './card-service.service';
import {HttpClientModule} from '@angular/common/http';
import {MatGridListModule} from '@angular/material/grid-list';
import { DeckComponent } from './deck/deck.component';
import { HeaderComponent } from './header/header.component';
import {Globals} from "./globals";
import { ModalCheckoutComponent, ModalCheckoutDialog } from './modal-checkout/modal-checkout.component';
import {MatDialogModule} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatInputModule, MatAutocompleteModule} from "@angular/material";

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {CardFilterPipe} from "./card-list/card-filter.pipe";
import {NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {ModalLoginComponent, ModalLoginDialog} from './modal-login/modal-login.component';
import {NgxPaginationModule} from "ngx-pagination";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";


@NgModule({
  declarations: [
    AppComponent,
    CardListComponent,
    DeckComponent,
    HeaderComponent,
    ModalCheckoutComponent,
    ModalCheckoutDialog,
      ModalLoginComponent,
      ModalLoginDialog,
    CardFilterPipe,
    ModalLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgbTooltipModule,
    MatAutocompleteModule,
    MatInputModule,
    NgxPaginationModule
  ],
  exports: [

    MatFormFieldModule,
  ],
  entryComponents: [ModalCheckoutComponent, ModalCheckoutDialog, ModalLoginComponent, ModalLoginDialog],
  providers: [CardServiceService,Globals,{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent, ModalCheckoutComponent, ModalLoginComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
