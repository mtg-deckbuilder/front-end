import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'cardFilter',
    pure: false
})
export class CardFilterPipe implements PipeTransform{
        transform(cards: any[], searchName: String, searchPower: String, searchToughness: String, searchCMC: String, searchColor: String, searchText: String, searchRarity: String, searchType: String){
            if (cards && cards.length) {
                return cards.filter(card => {
                    if (searchName && card.name.toLowerCase().indexOf(searchName.toLowerCase()) == -1) {
                        // console.log ("search Name")
                        return false;
                    }
                    if (searchPower && card.power.toString().toLowerCase().indexOf(searchPower.toString().toLowerCase()) == -1) {
                        // console.log ("search Power")
                        return false;
                    }
                    if (searchColor && card.color.toLowerCase().indexOf(searchColor.toLowerCase()) == -1){
                        // console.log ("search Color")
                        return false;
                    }
                    if (searchCMC && card.cmc.toString().toLowerCase().indexOf(searchCMC.toString().toLowerCase()) == -1){
                        // console.log ("search CMC")
                        return false;
                    }
                    if (searchType && card.type.toLowerCase().indexOf(searchType.toLowerCase()) == -1){
                        // console.log ("search Type")
                        return false;
                    }
                    if (searchRarity && card.rarity.toLowerCase().indexOf(searchRarity.toLowerCase()) == -1){
                        // console.log ("search Rarity")
                        return false;
                    }
                    if (searchToughness && card.toughness.toString().toLowerCase().indexOf(searchToughness.toString().toLowerCase()) == -1){
                        // console.log ("search Toughness")
                        return false;
                    }
                    if (searchText && card.text.toLowerCase().indexOf(searchText.toLowerCase()) == -1){
                        // console.log ("search Text")
                        return false;
                    }
                    return true;
                });
            } else {
            return cards;
            }
        }
}


//
// export class CardFilterPipe implements PipeTransform{
//     // transform(cards: any[], searchTerm: String): any[]{
//     transform(cards: any[], searchTerm: String){
//         if (cards && cards.length) {
//             return cards.filter(card => {
//                 if (searchTerm && card.name.toLowerCase().indexOf(searchTerm.toLowerCase()) == -1) {
//                     return false
//                 }
//                 return true;
//             });
//         } else {
//             return cards;
//         }
//     }
// }

// if (!cards || searchTerm){
//     return cards;
// }
//         return cards.filter(cards => cards.indexOf(searchTerm) !== -1);
//     }
// }
