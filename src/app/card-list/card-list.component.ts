import {Component, OnInit} from '@angular/core';
import {CardServiceService} from '../card-service.service';
import {Globals} from "../globals";


@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css'],

})
export class CardListComponent implements OnInit {
  private deckList: Array<any>;
  cards: Array<any>;
  searchName: String;
  searchText: String;
  searchPower: Number;
  searchToughness: Number;
  searchCMC: Number;
  searchColor: String;
  searchType: String;
  searchRarity: String;
  p: any;

  constructor(private CardService: CardServiceService, private globals: Globals) {
    this.deckList = globals.deckList;
  }

  ngOnInit() {
      this.CardService.getAllCards().subscribe(data => {
        this.cards = data;
      });

  }

  onSelect(card:any){
      this.deckList.push(card);
      this.globals.calculateTotal();
  }

  onEmptySelect(card:any) {
    if (this.globals.isAdmin) {
      this.onSelect(card);
    }
  }
  searchBarsAnimation() {
    document.getElementById("header-grid").classList.toggle("change");
    document.getElementById("search-text").classList.toggle("notVisible");
    document.getElementById("card-list-grid-container").classList.toggle("extend");
    document.getElementById("searchNameDiv").classList.toggle("notVisible");
    document.getElementById("searchPowerDiv").classList.toggle("notVisible");
    document.getElementById("searchToughnessDiv").classList.toggle("notVisible");
    document.getElementById("searchCmcDiv").classList.toggle("notVisible");
    document.getElementById("searchColorDiv").classList.toggle("notVisible");
    document.getElementById("searchTextDiv").classList.toggle("notVisible");
    document.getElementById("searchRarityDiv").classList.toggle("notVisible");
    document.getElementById("searchTypeDiv").classList.toggle("notVisible");
    this.searchName = "";
    this.searchCMC = undefined;
    this.searchColor = "";
    this.searchPower = undefined;
    this.searchRarity = "";
    this.searchText = "";
    this.searchToughness = undefined;
    this.searchType = "";
  }
}
