import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DeckService {

  constructor(private http: HttpClient) { }

  getAllDecks(): Observable<any> {
    return this.http.get('https://thebigdeck-backend-relaxed-raven.apps.pcf.pcfhost.com/deck');
  }
  createNewDeck(deck): Observable<any> {
    return this.http.post('https://thebigdeck-backend-relaxed-raven.apps.pcf.pcfhost.com/deck/add', deck);
  }
  getAllCardsInDecks(): Observable<any> {
    return this.http.get('https://thebigdeck-backend-relaxed-raven.apps.pcf.pcfhost.com/deck/cards');
  }
  addCardToDeck(cardInDeck): Observable<any> {
    return this.http.post('https://thebigdeck-backend-relaxed-raven.apps.pcf.pcfhost.com/deck/cards/add', cardInDeck);
  }
}
