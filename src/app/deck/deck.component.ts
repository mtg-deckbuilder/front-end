import {Component, Input, OnInit} from '@angular/core';
import {Globals} from '../globals'
import {CardServiceService} from "../card-service.service";
import {DeckService} from "../deck.service";
import {forEach} from "@angular/router/src/utils/collection";
import {FormControl, FormGroup} from "@angular/forms";
import {Observable} from "rxjs";


@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.css']
})
export class DeckComponent implements OnInit {
  MyControl = new FormControl();
  deckForm: FormGroup;
  deckList: Array<any>;
  cards: Array<any>;
  // @Input() childDeckList: Array<any>=[];
  cardsInDecks: Array<any>;
  deckID: bigint;
  decks: Array<any>;

  constructor(private DeckService: DeckService, private CardService: CardServiceService, public globals: Globals) {
    this.deckList = globals.deckList;
  }

  ngOnInit() {
    this.CardService.getAllCards().subscribe(data => {
      this.cards = data});
    this.DeckService.getAllDecks().subscribe(data => {
      this.decks = data});
    this.deckForm = new FormGroup( {
      deckName: new FormControl(),
      deckSelect: new FormControl()
    });
    this.DeckService.getAllCardsInDecks().subscribe(data => {
      this.cardsInDecks = data;
    });
  }

    OnSelectRemove(card:any) {

    let index = this.deckList.indexOf(card, 0);
      this.deckList.splice(index,1);
      this.globals.calculateTotal();
  }

  onCreateDeck() {
    if (this.deckForm.controls.deckName.value != null) {
      const deck = {
        name: this.deckForm.controls.deckName.value,
        owner: this.globals.userID ? {id: this.globals.userID} : null
      };
      this.DeckService.createNewDeck(deck).subscribe(data => {
        this.decks = data;
        this.deckID = this.decks[this.decks.length-1].id;
        console.log(this.deckID);
        this.saveDeck();
        this.deckForm.controls.deckSelect.patchValue(this.deckID);
      });
      this.deckForm.reset();
    } else if (this.deckForm.controls.deckSelect.value > 0) {
      this.deckID = this.deckForm.controls.deckSelect.value;
      console.log("deckID: " + this.deckID);
      for (let i=0; i<this.cardsInDecks.length; i++) {
        console.log("i: " + i);
        if (this.cardsInDecks[i].owner != null) {
          if (this.cardsInDecks[i].owner.id == this.deckID) {
            const card = {
              id: this.cardsInDecks[i].id
            };
            this.DeckService.addCardToDeck(card).subscribe(data => {
              this.cardsInDecks = data;
            });
          }
        }
      }
      this.saveDeck();
    }
  }
  saveDeck() {
    for (let i=0; i< this.deckList.length; i++) {
      const card = {
        owner: this.deckID ? {id: this.deckID} : null,
        cardInstance: this.deckList[i]
      };
      this.DeckService.addCardToDeck(card).subscribe(data => {
        this.cardsInDecks = data;
      });
    }
  }
  onOpenDeck() {
    this.deckID = this.deckForm.controls.deckSelect.value;
    this.DeckService.getAllCardsInDecks().subscribe(data => {
      this.cardsInDecks = data;
      this.loadDeck();
    });
  }
  loadDeck() {
    while (this.deckList.length>0) {
      this.deckList.splice(0,1);
    }
    for (let i=0; i<this.cardsInDecks.length; i++) {
      if (this.cardsInDecks[i].owner != null) {
        if (this.cardsInDecks[i].owner.id == this.deckID) {
          this.deckList.push(this.cardsInDecks[i].cardInstance);
        }
      }
    }
    this.globals.calculateTotal();
  }
}
