import {Injectable, NgModule} from '@angular/core';


@Injectable()
export class Globals {
    deckList: Array<any> = [];
    totalPrice: number = 0;
    isAdmin: boolean = false;
    isLoggedIn: boolean = false;
    userID: bigint;
    calculateTotal() {
        this.totalPrice = 0;
        for (let card of this.deckList) {
            // console.log(card.price);
            this.totalPrice+=card.price;
            // console.log(this.totalPrice);
        }
    }

}


