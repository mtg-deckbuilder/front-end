import {Component, OnChanges, OnInit} from '@angular/core';
import {Globals} from '../globals'
import {UserServiceService} from "../user-service.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // users: Array<any>;

  constructor(
      // private UserService: UserServiceService,
      public globals: Globals) {
  }

  ngOnInit() {
    // this.UserService.getAllUsers().subscribe(data => {
    //   this.users = data;
    //   console.log(this.users);
    // });
  }

  // userAdminAnimation() {
  //   const target2 = document.getElementById("header-admin-text");
  //   target2.classList.remove("dimmed");
  //   const target1 = document.getElementById("header-user-text");
  //   target1.classList.add("dimmed");
  //   this.globals.isAdmin = true;
  // }
  //
  // userUserAnimation() {
  //   const target2 = document.getElementById("header-admin-text");
  //   target2.classList.add("dimmed");
  //   const target1 = document.getElementById("header-user-text");
  //   target1.classList.remove("dimmed");
  //   this.globals.isAdmin = false;
  // }

}


