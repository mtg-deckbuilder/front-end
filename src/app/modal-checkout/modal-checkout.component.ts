import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {Globals} from "../globals";
import {SetInventoryServiceService} from "../set-inventory-service.service";

export interface DialogData {

}

@Component({
  selector: 'app-modal-checkout',
  templateUrl: './modal-checkout.component.html',
  styleUrls: ['./modal-checkout.component.css']
})
export class ModalCheckoutComponent {

  receipt: string;
  private deckList: Array<any>;

  constructor(public dialog: MatDialog, public globals: Globals) {
    this.deckList = globals.deckList;
  }

  openDialog(): void {
    this.printReceipt();
    let dialogRef = this.dialog.open (ModalCheckoutDialog, {
      width: '350px',
      data: {total: "$"+this.globals.totalPrice, receipt: this.receipt}
    });


  }

  printReceipt() {
    this.receipt = "<div class='container receipt-box'>";
    for (let card of this.deckList) {
      this.receipt += "<div class='left-element'>" + card.name + "</div><div class='right-element'>$" + card.price + "</div>";
    }
    this.receipt += "</div>";
  }

}

@ Component({
  selector: 'modal-checkout-dialog',
  templateUrl: 'modal-checkout-dialog.html',
})

export class ModalCheckoutDialog{

  // public total: string;
  // public receipt: string;
  //
  // ngOnInit() {
  //   this.total = this.data.total;
  //   this.receipt = this.data.receipt;
  // }

  constructor(
      public dialogRef: MatDialogRef<ModalCheckoutDialog>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData,
      public globals: Globals, private inventoryService: SetInventoryServiceService) {}


  onNoClick(): void {
    this.dialogRef.close();
  }

//   CountArray(arr: Array<any>, card: any){
//     let counts = 0;
//     for (var i = 0; i < arr.length; i++) {
//     var checkCard = arr[i];
//       console.log(counts);
//       console.log(checkCard.name);
//       console.log(card.name);
//     if (checkCard.name == card.name ){counts++}
// }
//     console.log(counts);
//     return counts;
//   }

  onYesClick(): void {
    let i = 0;
    let x = 0;
    let card: any;
    let adminMultiplier = -1;

    if (this.globals.isAdmin) {
      adminMultiplier = 1;
    }

    while (this.globals.deckList.length > 0) {
      card = this.globals.deckList[0];
      x = 0;
      while (this.globals.deckList.indexOf(card, 0) >= 0) {
        x += 1;
        let index = this.globals.deckList.indexOf(card, 0);
        this.globals.deckList.splice(index, 1);
      }
      // console.log(x);
      x*=adminMultiplier;
      console.log("X modal checkout: "+ x);


      this.inventoryService.ChangeQuantityToServer(card, x).subscribe();
      card.quantity += x;
      x=0;
    }


    //   x = this.CountArray(this.globals.deckList, card);
    //   this.inventoryService.ChangeQuantityToServer(this.globals.deckList[0], x).subscribe();
    //   for (i=0; i<x; i++){
    //     let index = this.globals.deckList.indexOf(card, 0);
    //     this.globals.deckList.splice(index,1);
    //   }
    // }



    // for (i ; i < (this.globals.deckList.length); i++) {
    //   card = this.globals.deckList[i];
    //   // console.log(card);
    //
    //   x = this.CountArray(this.globals.deckList, card);
    //   console.log(x);
    //
    //   this.inventoryService.ChangeQuantityToServer(this.globals.deckList[i], x).subscribe();
    // }
    //
    //
    // while (this.globals.deckList.length>0) {
    //   this.globals.deckList.splice(0,1);
    // }

    this.globals.calculateTotal();
    this.dialogRef.close();
  }
}
