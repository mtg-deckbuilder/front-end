import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {Globals} from "../globals";
import {UserServiceService} from "../user-service.service";

export interface DialogData {

}

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.css']
})
export class ModalLoginComponent implements OnInit {

  users: Array<any>;
  private userList: Array<any>;
  constructor(private UserService: UserServiceService, public dialog: MatDialog, public globals: Globals) {
  }

  ngOnInit() {
    this.UserService.getAllUsers().subscribe(data => {
      this.users = data;
    });
  }
  openDialog(): void {
    let dialogRef = this.dialog.open (ModalLoginDialog, {
      width: '225px',
      data: {total: "$"+this.globals.totalPrice}
    });
  }
  logout() : void {
      this.globals.isAdmin = false;
      this.globals.isLoggedIn = false;
      this.globals.userID = null;
      while (this.globals.deckList.length>0) {this.globals.deckList.splice(0,1);}
      this.globals.totalPrice = 0;
  }
}

@ Component({
  selector: 'modal-login-dialog',
  templateUrl: 'modal-login-dialog.html',
})

export class ModalLoginDialog implements OnInit {
  username: string;
  password: string;
  users: Array<any>;
  constructor(
      private UserService: UserServiceService,
      public dialogRef: MatDialogRef<ModalLoginDialog>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData,
      public globals: Globals) {}

  ngOnInit() {
    this.UserService.getAllUsers().subscribe(data => {
      this.users = data;
    });
  }

  login() : void {
    let valid = false;
    let i=0;
    // console.log(this.username);
    // console.log(this.password);
    for (i=0; i<this.users.length; i++) {
      // console.log(this.users[i].userName);
      // console.log(this.users[i].password);
      if (this.username == this.users[i].userName && this.password == this.users[i].password) {
        valid = true;
        if (this.users[i].type == "admin") {
          this.globals.isAdmin = true;
        }
        this.globals.userID = this.users[i].id;
        this.globals.isLoggedIn = true;
        this.dialogRef.close();
      }
    }
    if(!valid){
      alert("Invalid credentials");
    }
  }
}

