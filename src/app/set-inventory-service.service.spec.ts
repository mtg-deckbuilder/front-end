import { TestBed } from '@angular/core/testing';

import { SetInventoryServiceService } from './set-inventory-service.service';

describe('SetInventoryServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetInventoryServiceService = TestBed.get(SetInventoryServiceService);
    expect(service).toBeTruthy();
  });
});
