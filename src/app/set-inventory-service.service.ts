import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
// import any = jasmine.any;

@Injectable({
  providedIn: 'root'
})
export class SetInventoryServiceService {
  CardListForInventory: Array<any>;

  constructor(private http: HttpClient) { }


  ChangeQuantityToServer(card, x){
    console.log('serv'+card);
    // console.log('xval'+ x);
    console.log("Card Quantity: "+ card.quantity);
    console.log("X: "+ x);
    const ob={
      id: card.id,
      name: card.name,
      quantity: card.quantity+x,
      text: card.text,
      color: card.color,
          price: card.price,
        rarity: card.rarity,
        set: card.set,
        cmc: card.cmc,
        type: card.type,
      power: card.power,
      toughness: card.toughness,
      imgsource: card.imgsource
    }

    return this.http.post('https://thebigdeck-backend-relaxed-raven.apps.pcf.pcfhost.com/inventory', ob);
  }
}

