import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  constructor(private http: HttpClient) { }
  getAllUsers(): Observable<any> {
    return this.http.get('https://thebigdeck-backend-relaxed-raven.apps.pcf.pcfhost.com/user');
  }

}
